package com.demo.aswitch.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText username;
    private ImageView acc_del;
    private EditText password;
    private ImageView pas_del;
    private ImageView eye;
    private Button btn_login;
    private boolean beye;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        username = (EditText) findViewById(R.id.username);
        acc_del = (ImageView) findViewById(R.id.acc_del);
        password = (EditText) findViewById(R.id.password);
        pas_del = (ImageView) findViewById(R.id.pas_del);
        eye = (ImageView) findViewById(R.id.eye);
        btn_login = (Button) findViewById(R.id.btn_login);

        acc_del.setOnClickListener(this);
        pas_del.setOnClickListener(this);
        eye.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        //文本监视
        username.addTextChangedListener(new EditChangedListener(username));
        //焦点监视
        username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (TextUtils.isEmpty(password.getText().toString().trim())) {
                        setUsable(false);
                    }
                    if (!TextUtils.isEmpty(username.getText().toString().trim())) {
                        acc_del.setVisibility(View.VISIBLE);
                        pas_del.setVisibility(View.GONE);
                    }
                }
            }
        });
        //文本监视
        password.addTextChangedListener(new EditChangedListener(password));
        //焦点监视
        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (!TextUtils.isEmpty(password.getText().toString().trim())) {
                        acc_del.setVisibility(View.GONE);
                        pas_del.setVisibility(View.VISIBLE);
                    }
                    if (TextUtils.isEmpty(username.getText().toString().trim())) {
                        getFocus(username);
                        return;
                    }

                }
            }
        });
        //初始化不可用
        setUsable(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.acc_del:
                username.setText("");
                password.setText("");
                getFocus(username);
                break;
            case R.id.pas_del:
                getFocus(password);
                password.setText("");
                break;
            case R.id.eye:
                if (beye) {
                    eye.setImageResource(R.drawable.zy02_no);
                    //变为**
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    eye.setImageResource(R.drawable.zy02_yes);
                    //显示正常字符
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                beye = !beye;
                //刷新视图
                password.postInvalidate();
                //切换后将EditText光标置于末尾
                CharSequence charSequence = password.getText();
                if (charSequence instanceof Spannable) {
                    Spannable spanText = (Spannable) charSequence;
                    Selection.setSelection(spanText, charSequence.length());
                }
                break;
            case R.id.btn_login:

                break;
        }
    }

    private void submit() {
        // validate
        String usernameString = username.getText().toString().trim();
        if (TextUtils.isEmpty(usernameString)) {
            Toast.makeText(this, "手机号/用户名", Toast.LENGTH_SHORT).show();
            return;
        }

        String passwordString = password.getText().toString().trim();
        if (TextUtils.isEmpty(passwordString)) {
            Toast.makeText(this, "登录密码", Toast.LENGTH_SHORT).show();
            return;
        }
        // TODO validate success, do something
    }

    class EditChangedListener implements TextWatcher {
        private EditText et;

        public EditChangedListener(EditText et) {
            this.et = et;
        }

        /**
         * 文本改变之前的状态,将要改变
         *
         * @param s     文本
         * @param start 替换开始位置
         * @param count 要替换的字符数
         * @param after 替换为after个字符
         */
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            if (s.length() > 0) {
                acc_del.setVisibility(View.VISIBLE);
                pas_del.setVisibility(View.GONE);
            }

        }

        /**
         * 字符改变时的状态
         *
         * @param s      文本
         * @param start  替换开始位置
         * @param before 之前的字符数
         * @param count  添加的字符数 就是你一次输入的字符长度
         */
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.e("main", "onTextChanged---输入字符长度" + count);
            Log.e("main", "onTextChanged---" + s.toString());
        }

        /**
         * 字符改变后的状态,最终的文本s
         *
         * @param s
         */
        @Override
        public void afterTextChanged(Editable s) {
            Log.e("main", s.toString());

            if (et.getId() == R.id.username && s.length() == 11 && !Validator.isMobile(s.toString())) {
                Toast.makeText(LoginActivity.this, "手机号格式不正确!", Toast.LENGTH_SHORT).show();
                getFocus(et);

            }
            Log.e("main", "afterTextChanged---最终的字符长度" + s.length());
            if (s.length() == 0) {
                if (et.getId() == R.id.password) {
                    pas_del.setVisibility(View.GONE);
                } else {
                    acc_del.setVisibility(View.GONE);
                }
                setUsable(false);
            } else {
                if (et.getId() == R.id.password) {

                    acc_del.setVisibility(View.GONE);
                    pas_del.setVisibility(View.VISIBLE);

                    if (s.length() >= 6) {
                        setUsable(true);
                    } else {
                        setUsable(false);
                    }
                } else {

                    acc_del.setVisibility(View.VISIBLE);
                    pas_del.setVisibility(View.GONE);

                }
            }
        }

    }


    private void setUsable(boolean enabled) {
        if (enabled) {
            btn_login.setEnabled(enabled);
            btn_login.setBackgroundColor(getResources().getColor(R.color.red));
        } else {
            btn_login.setEnabled(enabled);
            btn_login.setBackgroundColor(getResources().getColor(R.color.color_h2));
        }

    }

    private void getFocus(EditText editText) {
        editText.setText("");
        editText.requestFocus();
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }
}
